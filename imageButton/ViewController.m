//
//  ViewController.m
//  imageButton
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/13.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Place the picture to be changed.
    [_smileBtn setImage:[UIImage imageNamed:@"smile2.jpg" ] forState:UIControlStateHighlighted];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
